package buu.phatcharapol.calculator

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import buu.phatcharapol.calculator.databinding.FragmentMinusBinding
import buu.phatcharapol.calculator.databinding.FragmentPlusBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

/**
 * A simple [Fragment] subclass.
 * Use the [MinusFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MinusFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private lateinit var binding: FragmentMinusBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_minus,
            container,
            false
        )
        var correct = MinusFragmentArgs.fromBundle(requireArguments()).correctScore
        var incorrect = MinusFragmentArgs.fromBundle(requireArguments()).incorrectScore
        var answer = game()

        binding.apply {
            val trueScore = binding.trueScore
            trueScore.setText("Correct: " + correct.toString())
            val falseScore = binding.falseScore
            falseScore.setText("Incorrect: " + incorrect.toString())
            btn1.setOnClickListener {
                if (btn1.text == answer.toString()) {
                    result.visibility = View.VISIBLE
                    result.text = "Correct Answer!"
                    result.setTextColor(Color.GREEN)
                    correct += 1
                    trueScore.text = "Correct: $correct"
                    Handler().postDelayed({
                        answer = game()
                    }, 1000)
                } else {
                    result.visibility = View.VISIBLE
                    result.text = "Incorrect Answer!"
                    result.setTextColor(Color.RED)
                    incorrect += 1
                    falseScore.text = "Incorrect: $incorrect"
                }
            }
            btn2.setOnClickListener {
                if (btn2.text == answer.toString()) {
                    result.visibility =
                        View.VISIBLE
                    result.text = "Correct Answer!"
                    result.setTextColor(Color.GREEN)
                    correct += 1
                    trueScore.text = "Correct: $correct"
                    Handler().postDelayed({
                        answer = game()
                    }, 1000)
                } else {
                    result.visibility = View.VISIBLE
                    result.text = "Incorrect Answer!"
                    result.setTextColor(Color.RED)
                    incorrect += 1
                    falseScore.text = "Incorrect: $incorrect"
                }
            }
            btn3.setOnClickListener {
                if (btn3.text == answer.toString()) {
                    result.visibility =
                        View.VISIBLE
                    result.text = "Correct Answer!"
                    result.setTextColor(Color.GREEN)
                    correct += 1
                    trueScore.text = "Correct: $correct"
                    Handler().postDelayed({
                        answer = game()
                    }, 1000)
                } else {
                    result.visibility =
                        View.VISIBLE
                    result.text = "Incorrect Answer!"
                    result.setTextColor(Color.RED)
                    incorrect += 1
                    falseScore.text = "Incorrect: $incorrect"
                }
            }
        }
        binding.btnExit.setOnClickListener{
            it.findNavController().navigate(
                MinusFragmentDirections.actionMinusFragmentToTitleFragment(
                    correct,
                    incorrect
                )
            )
        }
        return binding.root
    }

    private fun game(): Int {
        binding.apply {
        val num1 = num1
        val num2 = num2
        val btn1 = btn1
        val btn2 = btn2
        val btn3 = btn3
        val result = result
        result.setVisibility(View.INVISIBLE)
        val rannum1: Int = (0 until 10).random()
        val rannum2: Int = (0 until 10).random()
        num1.setText(rannum1.toString())
        num2.setText(rannum2.toString())

        val minus = rannum1 - rannum2
        val ranChoice: Int = (1 until 4).random()
        val minusMM = minus - 2
        val minusM = minus - 1
        val minusP = minus + 1
        val minusPP = minus + 2
        if (ranChoice == 1) {
            if(minus > -1){
                btn1.setText(minus.toString())
                btn2.setText(minusP.toString())
                btn3.setText(minusPP.toString())
            }else{
                btn1.setText(minus.toString())
                btn2.setText(minusP.toString())
                btn3.setText(minusPP.toString())
            }
        } else if (ranChoice == 2) {
                btn1.setText(minusM.toString())
                btn2.setText(minus.toString())
                btn3.setText(minusP.toString())
        } else if (ranChoice == 3) {
                btn1.setText(minusMM.toString())
                btn2.setText(minusM.toString())
                btn3.setText(minus.toString())
        }
            return minus;
        }
    }

}