package buu.phatcharapol.calculator

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import buu.phatcharapol.calculator.databinding.FragmentPlusBinding
import kotlinx.android.synthetic.main.fragment_plus.*
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

/**
 * A simple [Fragment] subclass.
 * Use the [PlusFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PlusFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private lateinit var binding: FragmentPlusBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_plus,
            container,
            false
        )
        var correct = PlusFragmentArgs.fromBundle(requireArguments()).correctScore
        var incorrect = PlusFragmentArgs.fromBundle(requireArguments()).incorrectScore
        var answer = game()

        binding.apply {
            val trueScore = binding.trueScore
            trueScore.setText("Correct: " + correct.toString())
            val falseScore = binding.falseScore
            falseScore.setText("Incorrect: " + incorrect.toString())
            btn1.setOnClickListener {
                if (btn1.text == answer.toString()) {
                    result.visibility = View.VISIBLE
                    result.text = "Correct Answer!"
                    result.setTextColor(Color.GREEN)
                    correct += 1
                    trueScore.text = "Correct: $correct"
                    Handler().postDelayed({
                        answer = game()
                    }, 1000)
                } else {
                    result.visibility = View.VISIBLE
                    result.text = "Incorrect Answer!"
                    result.setTextColor(Color.RED)
                    incorrect += 1
                    falseScore.text = "Incorrect: $incorrect"
                }
            }
            btn2.setOnClickListener {
                if (btn2.text == answer.toString()) {
                    result.visibility =
                        View.VISIBLE
                    result.text = "Correct Answer!"
                    result.setTextColor(Color.GREEN)
                    correct += 1
                    trueScore.text = "Correct: $correct"
                    Handler().postDelayed({
                        answer = game()
                    }, 1000)
                } else {
                    result.visibility = View.VISIBLE
                    result.text = "Incorrect Answer!"
                    result.setTextColor(Color.RED)
                    incorrect += 1
                    falseScore.text = "Incorrect: $incorrect"
                }
            }
            btn3.setOnClickListener {
                if (btn3.text == answer.toString()) {
                    result.visibility =
                        View.VISIBLE
                    result.text = "Correct Answer!"
                    result.setTextColor(Color.GREEN)
                    correct += 1
                    trueScore.text = "Correct: $correct"
                    Handler().postDelayed({
                        answer = game()
                    }, 1000)
                } else {
                    result.visibility =
                        View.VISIBLE
                    result.text = "Incorrect Answer!"
                    result.setTextColor(Color.RED)
                    incorrect += 1
                    falseScore.text = "Incorrect: $incorrect"
                }
            }
        }
            binding.btnExit.setOnClickListener{
            it.findNavController().navigate(
                PlusFragmentDirections.actionPlusFragmentToTitleFragment(
                    correct,
                    incorrect
                )
            )
        }
        return binding.root
    }

    private fun game(): Int {
        binding.apply {
        val num1 = num1
        val num2 = num2
        val btn1 = btn1
        val btn2 = btn2
        val btn3 = btn3
        val result = result
        result.setVisibility(View.INVISIBLE)
        val rannum1: Int = (0 until 10).random()
        val rannum2: Int = (0 until 10).random()
        num1.text=rannum1.toString()
        num2.text=rannum2.toString()

        val sum = rannum1 + rannum2
        val ranChoice: Int = (1 until 4).random()
        val sumMM = sum - 2
        val sumM = sum - 1
        val sumP = sum + 1
        val sumPP = sum + 2
        if (ranChoice == 1) {
            btn1.text = sum.toString()
            btn2.text = sumP.toString()
            btn3.text = sumPP.toString()
        } else if (ranChoice == 2) {
            btn1.text = sumM.toString()
            btn2.text = sum.toString()
            btn3.text = sumP.toString()
        } else if (ranChoice == 3) {
            btn1.text = sumMM.toString()
            btn2.text = sumM.toString()
            btn3.text = sum.toString()
        }
            return sum;
        }
    }
}